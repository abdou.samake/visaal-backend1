import * as functions from 'firebase-functions';
import {firestore} from "firebase-admin/lib/firestore";
import DocumentReference = firestore.DocumentReference;
import * as admin from "firebase-admin";
import {cert} from "./cred/cert";
import * as express from "express";
import * as cors from "cors";
import {PatientModel} from "./model/patient";
import DocumentSnapshot = firestore.DocumentSnapshot;

admin.initializeApp({
 credential: admin.credential.cert(cert),
 databaseURL: "https://visaal-fc520.firebaseio.com",
});

const app = express()
app.use(cors())

const db = admin.firestore();
const dataRefs = db.collection('patients')

/*export async function postNewPatient(newData: any, patientId: string): Promise<any> {
 if (!newData) {
  throw new Error(`new patient must be filled`);
 }

 const addResult: DocumentReference<DocumentData> = await dataRefs
     .doc(patientId)
     .add(newData)
 const createNewPatient: DocumentReference = dataRefs.doc(addResult.id);
 await createNewPatient.set({...newData, id: createNewPatient.id});
 return createNewPatient;
}*/

async function testIfPatientExistsById(patientId: string): Promise<DocumentReference> {
 const patientRef: DocumentReference = dataRefs.doc(patientId);
 const snapPatientToFind: DocumentSnapshot = await patientRef.get();
 const patientToFind: PatientModel | undefined = snapPatientToFind.data() as PatientModel | undefined;
 if (!patientToFind) {
  throw new Error(`${patientId} does not exists`);
 }
 return patientRef;
}

export async function updatePatient(patientId: string, newPatient: PatientModel): Promise<PatientModel> {
 if (!newPatient || !patientId) {
  throw new Error(`patient data and patient id must be filled`);
 }

 const patientToPatchRef: DocumentReference = await testIfPatientExistsById(patientId);
 await patientToPatchRef.update(newPatient);
 return newPatient;
}

/*pp.post('/:id', async (req, res) => {
 try {
  const newData = req.body;
  const patientId = req.params.id;
  const addResult = await postNewPatient(newData, patientId);
  return res.send(addResult)
 } catch (e) {
  return res.status(500).send({error: 'erreur serveur :' + e.message});
 }
})
*/
app.patch('/:id', async (req, res) => {
 try {
  const newPatient = req.body;
  const patientId = req.params.id
  const addResult = await updatePatient(patientId, newPatient);
  return res.send(addResult);
 } catch (e) {
  return res.status(500).send({error: 'erreur serveur :' + e.message});
 }
});

 export const do_create_data = functions.https.onRequest(app);
