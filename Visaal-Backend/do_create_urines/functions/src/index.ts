import * as functions from 'firebase-functions';
import {firestore} from "firebase-admin/lib/firestore";
import DocumentReference = firestore.DocumentReference;
const admin = require('firebase-admin')
import {cert} from "./cred/cert";
import * as express from "express";
import * as cors from "cors";
import DocumentSnapshot = firestore.DocumentSnapshot;

admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: "https://visaal-fc520.firebaseio.com",
});

const app = express()
app.use(cors())

const db = admin.firestore();
const patientRefs = db.collection('patients')
const dataUrineRefs = db.collection('urines')

export async function postNewUrineData(newUrineData: any, patientId: string): Promise<any> {
    if (!patientId || !newUrineData) {
        throw Error('hotelId and newRoom are missing')
    }
    const refPatient: DocumentReference = patientRefs.doc(patientId);
    const snapShotData: DocumentSnapshot = await refPatient.get();
    if (!snapShotData.exists) {
        return 'hotel dose not existe';
    }
    const createDataUrineRef: DocumentReference = await dataUrineRefs.add(newUrineData);
    const createDataUrineInPatient: DocumentReference = refPatient
        .collection('urines')
        .doc(createDataUrineRef.id);
    await createDataUrineInPatient.set({ref: createDataUrineInPatient, id: createDataUrineInPatient.id});
    return {...newUrineData, id: newUrineData.id};

}

app.post('/:id', async (req, res) => {
    try {
        const newPatient = req.body;
        const patientId = req.params.id;
        const addResult = await postNewUrineData(newPatient, patientId);
        return res.send(addResult);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});

 export const createUriens = functions.https.onRequest(app);
